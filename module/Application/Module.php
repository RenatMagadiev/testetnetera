<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Debug\Debug;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Permissions\Acl\Acl;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * Returns service manager configuration.
     *
     * Note: Here are factories that require instantiated classes because
     *       these can not be cached.
     *
     * @return array
     */
    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'dbAdapter' => function($sm) {
                    $config = $sm->get('config');

                    if (!array_key_exists('db', $config)) {
                        //print_dbconfig_error_and_die();
                        throw new \Exception('Unable to find configuration for database adapter!');
                    }

                    $dbAdapter = new \Zend\Db\Adapter\Adapter($config['db']);

                    return $dbAdapter;
                },
                'SearchModel' => function ($sm) {
                    $dbAdapter = $sm->get('dbAdapter');
                    return new \Application\Model\SearchModel($dbAdapter);
                }
            )
        );
    }


    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
}
