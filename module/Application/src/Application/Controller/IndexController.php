<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Application\Form\FilterForm;
use Zend\Debug\Debug;
use Zend\Http\Client;
use Zend\Json\Json;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Paginator\Paginator;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        $view = new ViewModel();

        $form = new FilterForm();

        if ($this->params()->fromQuery('username')) {
            $query = $this->params()->fromQuery();
            unset($query['page']);
            $form->setData($query);

            if ($form->isValid()) {

                $username = $form->get('username')->getValue();
                $page = $this->params()->fromQuery('page', 1);
                $this->getServiceLocator()->get('SearchModel')->insert($username, $this->getRequest()->getServer()->get('REMOTE_ADDR'));

                $httpAdapter = new \Zend\Paginator\Adapter\Callback(
                    function($offset, $itemCountPerPage) use ($username, $page)
                    {
                        $client = new Client(sprintf('https://api.github.com/users/%s/repos?page=%d&per_page=%d', $username, $page, $itemCountPerPage), array(
                            'adapter' => 'Zend\Http\Client\Adapter\Curl',
                            'maxredirects' => 0,
                            'sslverifypeer' => 0,
                            'timeout'      => 30,
                        ));
                        $data = Json::decode($client->send()->getBody());
                        return $data;
                    },
                    function() use ($username)
                    {
                        $client = new Client(sprintf('https://api.github.com/users/%s/repos', $username), array(
                            'adapter' => 'Zend\Http\Client\Adapter\Curl',
                            'maxredirects' => 0,
                            'sslverifypeer' => 0,
                            'timeout'      => 30,
                        ));
                        return count(Json::decode($client->send()->getBody()));
                    }
                );
                $paginator = new Paginator($httpAdapter);
                $paginator->setItemCountPerPage(5);
                $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
                $view->params = $this->params()->fromQuery();
                $view->paginator = $paginator;
            }
        }


        $view->form = $form;

        return $view;
    }
}
