<?php

namespace Application\Controller;

use Application\Form\DeleteForm;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Paginator\Paginator;
use Zend\View\Model\ViewModel;

class SearchController extends AbstractActionController
{
    public function indexAction()
    {
        $view = new ViewModel();
        /** @var Paginator $paginator */
        $paginator = $this->getServiceLocator()->get('SearchModel')->getUsernames();
        $paginator->setCurrentPageNumber($this->params()->fromQuery('page', 1));
        $paginator->setItemCountPerPage(5);

        $view->usernames = $paginator;

        return $view;
    }

    public function deleteAction()
    {
        $form = new DeleteForm();
        $view = new ViewModel();

        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );

            $form->setData($post);

            if ($form->isValid() && $form->get('password')->getValue() == 'tajemstvi') {
                $this->getServiceLocator()->get('SearchModel')->deleteOld($form->get('hours')->getValue());
                return $this->redirect()->toRoute('application/default', ['controller' => 'search']);
            }
        }
        $view->form = $form;

        return $view;
    }
}
