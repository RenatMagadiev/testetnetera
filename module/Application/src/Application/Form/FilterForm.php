<?php

namespace Application\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class FilterForm extends Form
{

    /**
     * @param string|null $name
     * @param array|null $options
     */
    public function __construct($name = 'login-form', $options = array())
    {
        parent::__construct($name, $options);
        $this->setAttribute('method', 'get');

        // username
        $this->add(array(
            'name' => 'username',
            'options' => array(
                'label' => 'Jméno uživatele',
                'required' => true
            ),
            'attributes' => array(
                'placeholder' => 'Vložte jméno uživatele…'
            )
        ));

        $this->add(new Element\Csrf('security'));

        $this->add(array(
            'type' => 'Submit',
            'name' => 'send',
            'attributes' => array(
                'value' => 'Hledat'
            )
        ));
    }

}
