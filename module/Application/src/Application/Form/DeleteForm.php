<?php

namespace Application\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class DeleteForm extends Form
{

    /**
     * @param string|null $name
     * @param array|null $options
     */
    public function __construct($name = 'delete-form', $options = array())
    {
        parent::__construct($name, $options);
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'hours',
            'options' => array(
                'label' => 'Počet hodin',
                'required' => true
            ),
            'attributes' => array(
                'placeholder' => 'Vložte počet hodin…'
            )
        ));

        $this->add(array(
            'name' => 'password',
            'options' => array(
                'label' => 'Heslo',
                'required' => true
            ),
            'attributes' => array(
                'placeholder' => 'Vložte "tajemstvi"…'
            ),
        ));

        $this->add(new Element\Csrf('security'));

        $this->add(array(
            'type' => 'Submit',
            'name' => 'send',
            'attributes' => array(
                'value' => 'Smazat'
            )
        ));
    }

}
