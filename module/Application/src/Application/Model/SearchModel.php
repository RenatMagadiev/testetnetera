<?php
namespace Application\Model;

use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\Adapter\Driver\Pdo\Result;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Debug\Debug;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

/**
 */
class SearchModel
{
    /**
     * @var string
     */
    protected $tableName = 'search';

    /**
     * @var \Zend\Db\Adapter\AdapterInterface
     */
    protected $dbAdapter;

    /**
     * @var \Zend\Db\Sql\Sql
     */
    protected $sql;

    /**
     * Constructor.
     *
     * @param \Zend\Db\Adapter\AdapterInterface $dbAdapter
     */
    public function __construct(AdapterInterface $dbAdapter)
    {
        $this->dbAdapter = $dbAdapter;
        $this->sql = new Sql($dbAdapter);

        if (!is_null($this->tableName) && !empty($this->tableName)) {
            $this->sql->setTable($this->tableName);
        }
    }

    /**
     * @return Result
     */
    public function getUsernames()
    {
        error_reporting(E_ALL);
        ini_set("show_errors", 1);
        $select = $this->sql->select();
        $select->order('inserted DESC');

        $results = new ResultSet();

        $paginatorAdapter = new DbSelect($select, $this->dbAdapter, $results);
        $paginator = new Paginator($paginatorAdapter);
        return $paginator;
    }

    public function insert($username, $ip)
    {
        $insert = $this->sql->insert();
        $insert->values(array('username' => $username, 'inserted' => date('Y-m-d H:i:s'), 'ip' => $ip));

        return $this->sql->prepareStatementForSqlObject($insert)->execute();
    }

    public function deleteOld($hours)
    {
        $delete = $this->sql->delete();
        $delete->where("inserted > '". date('Y-m-d H:i:s', strtotime('-'.$hours.' hour'))."'");

//        echo date('Y-m-d H:i:s', strtotime('-'.$hours.' hour'));
//        echo $delete->getSqlString();exit;

        return $this->sql->prepareStatementForSqlObject($delete)->execute();
    }

    /**
     * @return string Returns table name.
     */
    public function getTableName()
    {
        return $this->tableName;
    }
}
